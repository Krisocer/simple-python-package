-i https://pypi.org/simple
bleach==3.3.0; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3, 3.4'
build==0.3.1.post1
certifi==2020.12.5
chardet==4.0.0; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3, 3.4'
colorama==0.4.4; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3, 3.4'
docutils==0.17; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3, 3.4'
idna==2.10; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
importlib-metadata==3.10.0; python_version >= '3.6'
keyring==23.0.1; python_version >= '3.6'
packaging==20.9; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
pep517==0.10.0
pkginfo==1.7.0
pygments==2.8.1; python_version >= '3.5'
pyparsing==2.4.7; python_version >= '2.6' and python_version not in '3.0, 3.1, 3.2, 3.3'
pywin32-ctypes==0.2.0; sys_platform == 'win32'
readme-renderer==29.0
requests-toolbelt==0.9.1
requests==2.25.1; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3, 3.4'
rfc3986==1.4.0
six==1.15.0; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
toml==0.10.2; python_version >= '2.6' and python_version not in '3.0, 3.1, 3.2, 3.3'
tqdm==4.60.0; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
twine==3.4.1
urllib3==1.26.4; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3, 3.4' and python_version < '4'
webencodings==0.5.1
zipp==3.4.1; python_version >= '3.6'
